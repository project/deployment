<?php

/**
 * @file
 * Contains deployment.module.
 *
 * Includes some usefull functions for deployment and staging.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\locale\SourceString;

/**
 * Custom configuration importer.
 *
 * @param string $filename
 *   Configuration file name.
 * @param string|null $language
 *   Configuration language code.
 */
function deployment_config_import($filename, $language = NULL) {

  $update_callback = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
  $update_function = $update_callback[1]['function'];
  $update_file = $update_callback[0]['file'];
  $update_module = basename($update_file, '.install');
  $update_module_path = DRUPAL_ROOT . "/" . \Drupal::service('module_handler')->getModule($update_module)->getpath();
  $update = str_replace($update_module . "_", "", $update_function);
  $update_path = $update_module_path . "/deployment/" . $update;

  $source = new FileStorage($update_path);

  if ($value = $source->read($filename)) {
    /** @var \Drupal\Core\Config\ConfigManager $config_manager */
    $config_manager = \Drupal::service('config.manager');
    $entity_manager = $config_manager->getEntityTypeManager();
    $type = $config_manager->getEntityTypeIdByName($filename);
    if ($type !== NULL) {
      /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $entity_storage */
      $entity_storage = $entity_manager->getStorage($type);
      $entity = $config_manager->loadConfigEntityByName($filename);

      if ($entity) {
        $entity = $entity_storage->updateFromStorageRecord($entity, $value);
        $entity->save();
      }
      else {
        $entity = $entity_storage->createFromStorageRecord($value);
        $entity->save();
      }
    }
    else {
      $config_storage = \Drupal::service('config.storage');
      $config_storage->write($filename, $value);
    }
  }
  else {
    \Drupal::logger('deployment')->warning('Missing config file: ' . $filename . ' - not imported');
  }
}

/**
 * Custom configuration delete.
 *
 * @param string $filename
 *   Configuration file name.
 */
function deployment_config_delete($filename) {
  \Drupal::service('config.factory')->getEditable($filename)->delete();
}

/**
 * Custom translation import.
 *
 * @param string $source_string
 *   Source.
 * @param string $langcode
 *   Language.
 * @param string $translated_string
 *   Destination.
 */
function deployment_translation_add($source_string, $langcode, $translated_string) {
  $moduleHandler = \Drupal::service('module_handler');
  if ($moduleHandler->moduleExists('locale')) {
    // Find existing source string.
    $storage = \Drupal::service('locale.storage');
    $string = $storage->findString(['source' => $source_string]);
    if (is_null($string)) {
      $string = new SourceString();
      $string->setString($source_string);
      $string->setStorage($storage);
      $string->save();
    }
    // Create translation. If one already exists, it will be replaced.
    $translation = $storage->createTranslation([
      'lid' => $string->lid,
      'language' => $langcode,
      'translation' => $translated_string,
    ])->save();
  }
  else {
    \Drupal::logger('deployment')->warning('Module "locale" is not installed!');
  }
}

/**
 * Custom module installer.
 *
 * @param string $module
 *   Module name.
 */
function deployment_module_install($module) {
  \Drupal::service('module_installer')->install([$module]);
}

/**
 * Custom module uninstaller.
 *
 * @param string $module
 *   Module name.
 */
function deployment_module_uninstall($module) {
  \Drupal::service('module_installer')->uninstall([$module]);
}

/**
 * Custom Logger.
 *
 * @param string $message
 *   Message string.
 */
function deployment_log($message) {
  if (is_array($message)) {
    $message = print_r($message, 1);
  }
  \Drupal::logger('deployment')->notice($message);
}
