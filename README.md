[[_TOC_]]

# Deployment

Helper functions for deployment and staging.<br>
Usually called with update-hooks from "drush updb".

Create your own small module like "deployment_example", where you can use<br>
your own update-hooks (yourmodule_update_XXXX()).

## Functions
 
### Configuration handling

- deployment_config_import('dummy.config');<br>
If the update function is "your_module_update_8001()",<br>
the config-file is located in "your_module/deployment/update_8001/".<br>
Pay attention:<br>
If the config is "dummy.config", the file is "dummy.config.**yml**".
- deployment_config_delete('dummy.config');

### Translation handling

- deployment_translation_add("Dummy", "de", "NoDummy");<br>
To use this functionality you need to have **locale** module enabled.

### Module handling

- deployment_module_install("module_filter");
- deployment_module_uninstall("module_filter");
